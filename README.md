Docking Station Controller Setup
==============================

This repo contains [Ansible](http://docs.ansible.com) playbooks
for the configuring the OS installation on the Docking Station
Controller (DPC).

The DSC is a [Technologic Systems](http://www.embeddedarm.com)
TS-4800/8200 compact computer running Debian Linux (version 6).
The vendor's OS installation needs some customization before
it can be used for the DSC.

## Installation

You will need to be familiar with Ansible before trying this.

1. Clone this repo to the system that serves as your Ansible control
machine.
2. Create a set of SSH host-keys on the DSC and restart the SSH server. This
will need to be done from the system console.

```
ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa
ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa
/etc/init.d/ssh restart
```

3. Configure the root account on the DSC to allow SSH logins with a
public key.
4. Edit `/etc/apt/sources.list` and change the URL for the Debian
repository to:

```
http://archive.debian.org/debian-archive/debian
```

5. Install Python on the DSC

```
apt-get update
apt-get install python-dev
```

6. From the repo directory, create an inventory file with the
following contents, replacing *HOSTNAME* with the desired host-name
for the DSC and *IPADDR* with its IP address.

```
[dsc]
HOSTNAME ansible_ssh_host=IPADDR ansible_ssh_user=root hostname=HOSTNAME
```

7. Run Ansible from the repo directory.

```
ansible-playbook -i inventory_file provisioning.yaml
```
